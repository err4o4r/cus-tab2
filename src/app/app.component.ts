import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public myArr = [
    {
      id:0,
      title:1
    },
    {
      id:1,
      title:1
    },
    {
      id:2,
      title:1
    }
  ]
  public myStateArr: (number|null)[] = [];

  f(index: number, state: number) {
    this.myStateArr[index] === state ?
      this.myStateArr[index] = null :
      this.myStateArr[index] = state;
  }
}
