import {AfterContentInit, Directive, ElementRef, Input, Renderer2} from '@angular/core';
import {concatMap, distinctUntilChanged, fromEvent, map, Observable, scan, throttleTime, windowTime} from "rxjs";
import {ICollapse, ICusTab} from "./app.interface";
import {CusTabService} from "./cus-tab.service";

@Directive({
  selector: '[appCusTab]',
  providers: [CusTabService]
})
export class CusTabDirective implements AfterContentInit {

  @Input()
  cusActiveClass: string | null = null;

  public contents!: HTMLElement[];
  public tabs: ICusTab[] = [];
  public elements: HTMLElement[] = [];

  constructor(private elementRef: ElementRef,
              private renderer: Renderer2,
              private cusTabService: CusTabService
  ) {
  }

  public clickEvent$ = new Observable<Event>();

  ngAfterContentInit(): void {

    this.tabs = this.cusTabService.tabs;
    this.elements = this.tabs.map(item => item.elementRef.nativeElement);
    for (let item of this.tabs) {
      if (item.isActive) {
        item.viewContainerRef.createEmbeddedView(item.templateRef);
        if (this.cusActiveClass) {
          this.renderer.addClass(item.elementRef.nativeElement, this.cusActiveClass);
        }
        break;
      }
    }
    this.clickEvent$ =
      fromEvent(this.elements, 'click');

    this.clickEvent$.pipe(
      throttleTime(300),
      map((e: Event): ICusTab => (
          this.tabs.find(
            (item) =>
              (e.target as HTMLElement) === item.elementRef.nativeElement
          ) ?? this.tabs[0]
        )
      ),
      windowTime(500),
      concatMap((obs) =>
        obs.pipe(
          distinctUntilChanged((prev, curr) => prev.id === curr.id)
        )
      ),
      scan((acc: ICollapse, curr: ICusTab): ICollapse => {
          return (
            curr?.id === acc.open?.id ?
              {close: curr, open: null} :
              {close: acc?.open, open: curr}
          );
        }, {open: this.tabs.find(item => item.isActive) ?? null, close: null}
      ),
    ).subscribe((item) => {

      if (item.close) {
        item.close?.viewContainerRef.clear();
        if (this.cusActiveClass) {
          this.renderer.removeClass(item.close?.elementRef.nativeElement, this.cusActiveClass);
        }
      }
      if (item.open) {
        item.open?.viewContainerRef.createEmbeddedView(item.open?.templateRef);
        if (this.cusActiveClass) {
          this.renderer.addClass(item.open?.elementRef.nativeElement, this.cusActiveClass);
        }
      }
    })
  }
}
