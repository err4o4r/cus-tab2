import {Injectable} from '@angular/core';
import {ICusContentTab, ICusHeaderTab, ICusTab} from "./app.interface";

@Injectable()
export class CusTabService {

  private _headerTabs: ICusHeaderTab[] = [];
  private _contentTabs: ICusContentTab[] = [];
  private _tabs: ICusTab[] = [];

  constructor() {
  }

  public registerHeaderTab(header: ICusHeaderTab): void {
    this._headerTabs.push(header);
  }

  public registerContentTab(content: ICusContentTab): void {
    this._contentTabs.push(content);
  }

  get contentTabs(): ICusContentTab[] {
    return this._contentTabs;
  }

  get headerTabs(): ICusHeaderTab[] {
    return this._headerTabs;
  }

  get tabs(): ICusTab[] {
    if(this._tabs.length === 0){
      let temp;
      for (const item of this._headerTabs) {
        temp = this._contentTabs.find((item2) => {
          return item.id === item2.id;
        });
        if (temp) {
          this._tabs.push({...temp, ...item})
        }
      }
    }
    return this._tabs;
  }

}
