import {ContentChild, Directive, Host, Input, OnInit, TemplateRef, ViewContainerRef} from '@angular/core';
import {CusTabService} from "./cus-tab.service";

@Directive({
  selector: '[appCusContentTab]'
})
export class CusContentTabDirective implements OnInit  {

  @Input('appCusContentTab')
  cusTabId: string = '';

  @ContentChild(HTMLElement)
  wrapperEl!: HTMLElement;


  constructor(private viewContainerRef: ViewContainerRef,
              @Host() private cusTabService: CusTabService,
              private templateRef: TemplateRef<any>) {
  }

  ngOnInit(): void {
    this.cusTabService.registerContentTab(
      {
        templateRef: this.templateRef,
        viewContainerRef: this.viewContainerRef,
        id: this.cusTabId
      }
    )
  }

}
