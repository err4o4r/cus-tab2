import {Directive, ElementRef, Host, Input, OnInit} from '@angular/core';
import {CusTabService} from "./cus-tab.service";

@Directive({
  selector: '[appCusHeaderTab]'
})
export class CusHeaderTabDirective implements OnInit {

  @Input('appCusHeaderTab')
  cusTabId: string = '';

  @Input()
  cusActive: boolean = false;


  constructor(@Host() private cusTabService: CusTabService,
              private elementRef: ElementRef
  ) {
  }

  ngOnInit(): void {
    this.cusTabService.registerHeaderTab(
      {
        elementRef: this.elementRef,
        id: this.cusTabId,
        isActive: this.cusActive
      }
    );
  }

}
